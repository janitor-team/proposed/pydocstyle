Source: pydocstyle
Section: devel
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: ChangZhuo Chen (陳昌倬) <czchen@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3,
               python3-mock,
               python3-pytest,
               python3-setuptools,
               python3-snowballstemmer,
               python3-sphinx,
Standards-Version: 4.1.4
Homepage: https://pydocstyle.readthedocs.org/
Vcs-Git: https://salsa.debian.org/python-team/packages/pydocstyle.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pydocstyle

Package: pydocstyle
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-pydocstyle (= ${binary:Version}),
Conflicts: pep257
Replaces: pep257
Description: Python docstring style checker (PEP-257 conventions)
 PEP-257 provides conventions for Python docstrings (string literals which
 occur as first statement in a module, function, class or method definition
 for documentation purposes). This tool checks Python code whether
 these conventions have been complied with, and if docstring are missing.
 .
 This is a successor of the application "pep257".
 .
 This package installs the cli tool.

Package: python3-pydocstyle
Section: python
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
Conflicts: pep257
Breaks: pydocstyle (<< 2.0.0-1~exp2)
Replaces: pep257,
          pydocstyle (<< 2.0.0-1~exp2),
Description: Python docstring style checker (Python 3 library)
 PEP-257 provides conventions for Python docstrings (string literals which
 occur as first statement in a module, function, class or method definition
 for documentation purposes). This tool checks Python code whether
 these conventions have been complied with, and if docstring are missing.
 .
 This is a successor of the application "pep257".
 .
 This package installs the library for Python 3.
